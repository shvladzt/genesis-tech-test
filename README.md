# Genesis Technical 



## Getting started
Pull project and run given commands inside root folder to build the project
```
docker-compose build && docker-compose up -d
composer install
composer dump -o
```

```
 Change the host and port settings based on whether you are using docker or local environment in /config/redis.php  
 Default there will be local env
```
## Available commands
Inside the root folder you have two main commands

First to create message in a given time ( time pattern  t:m -> 14:00 )
```
php console create-message <"message"> <time>
// php console create-message "Hello world!" 15:36
```
Second command to start php worker process for a reading those messages
```
php console start-worker
```



#TODO

1. UNIT testing
2. Todo lists