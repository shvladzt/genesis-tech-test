<?php

return [
//    'host' => 'docker-redis',
//    'port' => 6379,
    'host' => '127.0.0.1',
    'port' => 6382,
    'connectTimeout' => 2.5,
    'auth' => ['phpredis', 'phpredis'],
    'ssl' => ['verify_peer' => false],
];