<?php
namespace App\Workers\CliMessage;

require_once __DIR__ . '' . '/../../../vendor/autoload.php';

use App\Services\CliMessage\CliMessageService;
use App\Services\Pool\PoolService;
use App\Services\Redis\RedisService;
use Carbon\Carbon;
use Exception;
use RedisException;

class CliMessageWorker
{
    public int $worker_id;

    public string $worker_hash;

    public int $worker_version;

    public string $start_time;

    public string $end_time;

    public string $time_limit;

    public bool $run = true;

    public string $status = 'init';



    private PoolService $poolService;
    private RedisService $redisService;


    /**
     * @throws Exception
     */
    public function __construct()
    {
        $worker_id = 1;
        if(!is_numeric($worker_id) || $worker_id <= 0) {
            throw new Exception("Invalid Worker ID: $worker_id");
        }

        $this->poolService  = new PoolService();
        $this->redisService = new RedisService();

        $this->worker_id    = $worker_id;

        $this->log("Constructing Worker [$worker_id] ... ");

        $this->worker_version = 1;
        $this->worker_hash  = md5(rand(0,99999999999));     // Assign random MD5 Hash
        $this->start_time   = time();
        $this->time_limit   = 60 * 60 * 1;                  // Minimum of 1 hour
        $this->time_limit   += rand(0, 60 * 30);            // Adding additional time between 0 and 30 minutes
        $this->end_time = $this->start_time + $this->time_limit;


        $this->log("Worker Hash: {$this->worker_hash}");
    }



    public function setup(): static
    {
        $this->log("Timeout Set to 0");
        set_time_limit(0);

        $this->log("Setup Complete");

        return $this;
    }



    /**
     * @throws RedisException
     */
    public function run()
    {
        $this->Log("Beginning to Run");

        try {
            while($this->run) {
                $this->status = 'listening';
                $this->report();

                $pool = array_reverse( $this->poolService->getPools( CliMessageService::DEFAULT_MESSAGE_LIST.':*' ) );

                // TODO: you can refactor this, use PoolService class
                if( isset($pool) && sizeof($pool) != null){

                    $pool = $pool[0];

                    $poolTimestamp = substr($pool, strpos($pool, ":") + 1);
                    $poolTime = Carbon::createFromTimestamp( $poolTimestamp );

                    if( $poolTime <= Carbon::parse(date("Y-m-d H:i:s")) ) {
                        $this->processPool( $pool );
                    }
                }


                sleep(1);
                echo "[".date("H:i:s")."] ".PHP_EOL;

                $this->status = 'checking';
                $this->report();
                $this->checkStatus();
            }

        }
        catch (Exception $ex) {
            $this->Log("Exception Caught: ".$ex->getMessage());
            var_dump($ex->getTrace());
        }

        $this->cleanup();

        return;
    }


    /**
     * Process a job.
     * @param string $poolIdentifier
     * @throws RedisException
     */
    public function processPool( string $poolIdentifier )
    {
        $poolData = array_reverse( $this->poolService->getPoolData( $poolIdentifier ) );

        foreach ( $poolData as $message ){
            echo "[".date("H:i:s")."] MESSAGE: ".$message.' '.PHP_EOL;
            $this->redisService->removeFieldFromList( $poolIdentifier, $message );
        }
    }

    /**
     * @throws RedisException
     */
    public function checkStatus()
    {
        if(time() > $this->end_time) {
            $this->Log("Worker has passed end time. Running Stopped.");
            $this->run = false;
        }

        //TODO: make it
//        $current_version = $this->redis->get('worker.version');
        $current_version = 1;

        if($this->worker_version != $current_version){
            $this->Log("Worker Version has change from {$this->worker_version} to {$current_version}. Running Stopped.");
            $this->run = false;
        }

        $this->report();
    }


    /**
     * @throws RedisException
     */
    public function cleanup()
    {
        $this->status = 'offline';
        $this->report();

        unset($this->redisService);
        unset($this->poolService);
    }

    /**
     * @throws RedisException
     */
    public function report()
    {
        $json = json_encode(array(
            "worker_id" => $this->worker_id
        ,"worker_hash" => $this->worker_hash
//        ,"worker_version" => $this->worker_version
        ,"time_limit" => $this->time_limit
        ,"end_time" => $this->end_time
        ,"status" => $this->status
        ));

        $this->redisService->addFieldToHash('worker.status', $this->worker_id, $json);
    }

    /**
     * Method for logging out to the console
     *
     * @param string $txt
     */
    public function log( string $txt )
    {
        echo "[".date("H:i:s")."] ".$txt."\n";
    }

}

try {
    $worker = new CliMessageWorker();
    $worker->Setup()->Run();
} catch (Exception $e) {
    throw new $e;
}

