<?php
namespace App\Services\Pool;

use App\Services\Redis\RedisService;

class PoolService
{
    private RedisService $redisService;

    public function __construct()
    {
        $this->redisService = new RedisService();
    }

    /**
     * @throws \RedisException
     */
    public function getPools(string $pattern ): array
    {
        return $this->redisService->getPools( $pattern );
    }

    //TODO: можно использовать type для определения какой метод юзать на пулле для того что бы не писать под каждый пулл отдельно метод
    /**
     * @throws \RedisException
     */
    public function getPoolData( string $poolIdentifier ): array
    {
        return $this->redisService->getListFields( $poolIdentifier );
    }



}