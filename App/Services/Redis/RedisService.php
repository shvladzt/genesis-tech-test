<?php

namespace App\Services\Redis;

use Predis\Client;

final class RedisService
{

    private Client $client;

    public function __construct()
    {
        $redisConfig = include(__DIR__ . '/../../../config/redis.php');

        $this->client = new Client( $redisConfig );

        $this->client->connect();
    }


    /**
     * @throws \RedisException
     */
    public function getValue(string $poolIdentifier, string $key ): ?string
    {
        try {
            return $this->client->hget( $poolIdentifier,  $key );
        }catch (\RedisException $exception ){
            throw new $exception;
        }
    }

    /**
     * @throws \RedisException
     */
    public function getPools(string $pattern ): array
    {
        try {
            return $this->client->keys( $pattern );
        }catch ( \RedisException $exception ){
            throw new $exception;
        }
    }


    /**
     * @throws \RedisException
     */
    public function getListFields(string $poolIdentifier ): array
    {
        try {
            return $this->client->lrange( $poolIdentifier, 0, $this->client->llen( $poolIdentifier ) );
        }catch ( \RedisException $exception ){
            throw new $exception;
        }
    }

    /**
     * @throws \RedisException
     */
    public function addFieldToList(string $poolIdentifier, string $field ): void
    {
        try {
            $this->client->lpush( $poolIdentifier, [$field] );
        }catch ( \RedisException $exception ){
            throw new $exception;
        }
    }

    /**
     * @throws \RedisException
     */
    public function addFieldToHash(string $poolIdentifier, string $field, string $value): void
    {
        try {
            $this->client->hset( $poolIdentifier, $field, $value );
        }catch ( \RedisException $exception ){
            throw new $exception;
        }
    }

    /**
     * @throws \RedisException
     */
    public function removeFieldFromList(string $poolIdentifier, string $field ): void
    {
        try {
            $this->client->lrem( $poolIdentifier, 1, $field );
        }catch ( \RedisException $exception ){
            throw new $exception;
        }
    }

    /**
     * @throws \RedisException
     */
    public function removeFieldFromHash(string $poolIdentifier, string $field ): void
    {
        try {
            $this->client->hdel( $poolIdentifier,  [$field] );
        }catch ( \RedisException $exception ){
            throw new $exception;
        }
    }

    public function __destruct()
    {
        $this->client->disconnect();
    }

}