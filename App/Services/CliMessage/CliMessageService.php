<?php

namespace App\Services\CliMessage;

use App\Services\Redis\RedisService;
use Carbon\Carbon;

class CliMessageService
{

    const
        DEFAULT_MESSAGE_LIST    = 'cli_messages',
        USED_MESSAGE_LIST       = 'cli_messages-used-messages';

    private RedisService $redisService;

    public function __construct()
    {
        $this->redisService = new RedisService();
    }


    /**
     * Current method checks if given message already existed
     * If so - then method will try to destroy old message in old pool of messages
     * and after that will create new one in new messagePool
     * //TODO: create some sort of MessageModel with toRedis method in it
     * @throws \Exception
     */
    public function create( string $message, string $showTime ): void
    {

        if( $messagePool = $this->getMessagePoolIdentifier( $message ) ) {
            $this->removeOldPoolMessage( $messagePool, $message );
            $this->redisService->removeFieldFromHash( $messagePool, $message );
        }

        $this->registerMessage( $message , $showTime );
    }

    /**
     * @throws \RedisException
     */
    public function getMessages( string $poolIdentifier ): array
    {
        return $this->redisService->getListFields( $poolIdentifier );
    }

    /**
     * @throws \RedisException
     */
    public function getAvailableMessagePools(): array
    {
        return $this->redisService->getPools( self::DEFAULT_MESSAGE_LIST.':*' );
    }


    /**
     * Method to retrieve identifier to a pool from message saved in redis table
     * @throws \RedisException
     */
    private function getMessagePoolIdentifier( string $message  ): ?string
    {
        return $this->redisService->getValue( self::USED_MESSAGE_LIST, $message );
    }


    /**
     * Current method is removing duplicated message from old pool
     * to make register same message in new pool of messages
     * @throws \RedisException
     */
    private function removeOldPoolMessage( string $poolIdentifier, string $message ): void
    {
        $this->redisService->removeFieldFromList( $poolIdentifier, $message );
    }

    /**
     *  Save message to message pool for worker
     * @throws \RedisException
     */
    private function registerMessage(string $message, string $showTime ): void
    {
        $poolIdentifier = self::DEFAULT_MESSAGE_LIST.':'.Carbon::parse( $showTime )->timestamp;

        // save message in 'used-messages' table
        $this->redisService->addFieldToHash(
            self::USED_MESSAGE_LIST,
            $message,
            $poolIdentifier
        );

        $this->redisService->addFieldToList( $poolIdentifier, $message );

    }



}