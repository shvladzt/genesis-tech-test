<?php
namespace App\Commands\CliMessages;


use App\Services\Redis\RedisService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetAllMessages extends Command
{
    private RedisService $redisService;

    public function __construct()
    {
        $this->redisService = new RedisService();
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('cli-get-messages')
            ->setDescription('Gets all cli messages!');
    }

    /**
     * @throws \Predis\ClientException
     */
    protected function execute( InputInterface $input, OutputInterface $output ): int
    {
        $output->writeln( 'Results by the key: ');
        $output->writeln(
            json_encode( $this->redisService->getValuesFromKey('cli_messages') )
        );

        return true;
    }
}