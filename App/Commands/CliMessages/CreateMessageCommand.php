<?php

namespace App\Commands\CliMessages;

use Carbon\Carbon;
use App\Services\CliMessage\CliMessageService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateMessageCommand extends Command
{

    private CliMessageService $messageService;


    public function __construct()
    {
        $this->messageService = new CliMessageService();
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('create-message')
            ->setDescription('Saves the given message ')
            ->setHelp('This command saves given message and then will display it at a given time!')
            ->addArgument('message', InputArgument::REQUIRED, 'Set message.')
            ->addArgument('time', InputArgument::REQUIRED, 'Set output time')
            ->addArgument('date', InputArgument::OPTIONAL, 'Set date');
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $selectedTime = $this->validateGivenTime( $input->getArgument('time'), );

        $this->messageService->create( $input->getArgument('message'), $selectedTime );

        $output->writeln('Given message: '.$input->getArgument('message'));
        $output->writeln('Output will be at -> '. $selectedTime );

        return true;
    }


    //TODO: make validation class
    /**
     * @throws \Exception
     */
    protected function validateGivenTime( string $payload, string $date = null ): Carbon
    {
        try {
            $givenDateTime = Carbon::parse( $payload.' '.$date );

            if( $givenDateTime < Carbon::now() )
                throw new \Exception('You cant choose past time');

            return $givenDateTime;
        }catch ( \Throwable $exception ){
            throw new \Exception( $exception->getMessage() );
        }
    }
}