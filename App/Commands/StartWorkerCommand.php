<?php

namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StartWorkerCommand extends Command
{
    const
        CLI_MESSAGE_WORKER = __DIR__.'/../Workers/CliMessage/CliMessageWorker.php';

    public int $initWorkerId;


    protected function configure()
    {
        $this->setName('start-worker')
            ->setDescription('Starts php worker!');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        include self::CLI_MESSAGE_WORKER;
        return true;
    }
}